Why Dependent Types Matter
==========================

Companion code in Agda, Idris ans Haskell for "Why Dependent Types Matter" paper
by Thorsten Altenkirch, Conor McBride and James McKinna.  Original code in the
paper uses Epigram language, which is no longer maintained or even available.
Thus I made a rewrite to modern functional languages.  For more information see
blog posts about
[Agda code](https://jstolarek.github.io/posts/2013-11-07-why-dependent-types-matter-in-agda.html),
[Idris code](https://jstolarek.github.io/posts/2013-12-02-idris-first-impressions.html) and
[Haskell code](https://jstolarek.github.io/posts/2014-12-09-why-dependent-types-matter-in-haskell.html).
